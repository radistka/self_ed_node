
module.exports = {
    '.css': (function() {
        return {"Content-Type": "text/css"}
    })(),
    '.jpg': (function() {
        return {"Content-Type": "image/jpg"}
    })(),
    '.html': (function() {
        return {"Content-Type": "text/html"}
    })
};