var http = require('http'),
    path = require('path'),
    fs = require('fs'),
    typesHandler = require('./types_handler.js');

var server = http.createServer(function(req,res) {
    var type = path.extname(req.url);
    var currentPath = './static/public' + req.url;
    if(req.url == '/') {
        currentPath = './static/public/index.html';
        type = '.html'
    }
    fs.readFile(currentPath, function(err, data) {
        if(!err) {
            res.writeHead(200, typesHandler[type]);
            res.write(data);
            res.end();
        }
    });

});



server.listen(8008);

console.log('server running at port - 8008');