
module.exports = {
    '/hello': sayHello,
    '/bye': sayBye
};

function sayHello(url) {
    var name = url.query['name'];
    if(name) {
        return 'Hello, ' + name + '!';
    }
    return 'Hello!'

}
function sayBye() {
    return 'Bye,bye!'
}