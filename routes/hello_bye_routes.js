var http = require('http'),
    url = require('url'),
    requestHandler = require('./request_handler');

var server = http.createServer(function(req,res) {
    var currentUrl = url.parse(req.url, true);
    if(requestHandler[currentUrl.pathname]) {
        var responseText = requestHandler[currentUrl.pathname](currentUrl);
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.end(responseText);
    } else{
        res.writeHead(404, {"Content-Type": "text/plain"});
        res.end("404 Not found");
    }
});

server.listen(8008);

console.log('server running at port - 8008');