var http= require('http'),
    fs = require('fs'),
    qs = require('querystring');

var server = http.createServer(function(req,res) {
    if(req.method == 'POST') {
        var body = '';
        req.on('data', function(data) {
            body += data;
        });
        req.on('end', function() {
            var name = qs.parse(body)['name'];
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.end('Hello, ' + name + '!');
        })
    }
    fs.readFile('./form.html', function(err, data){
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(data);
        res.end();
    });
});

server.listen(8008);

console.log('server running at port - 8008');